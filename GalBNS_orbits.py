import numpy as np
from numpy.random import uniform

import matplotlib.pyplot as plt

from scipy.spatial.transform import Rotation
from scipy.interpolate import interp1d

import astropy.units as u
import astropy.coordinates as coord
from astropy.coordinates import Angle

from galpy.potential.mwpotentials import McMillan17, MWPotential2014
from galpy.potential import vcirc
from galpy.orbit import Orbit

def trunc_normal(loc, scale, size):

    sample = []
    while len(sample) < size:
        while True:
            s = np.random.normal(loc, scale)
            if 0 <= s:
                break
        sample.append(s)

    return np.array(sample)
    
def v_along_n(v,n):

    v_proj = np.zeros(v.shape)
    for ii in range(len(v_proj)):
        v_proj[ii] = v[ii].dot(n[ii]) * n[ii]

    return v_proj

def get_inputs(raw, bns_index):

    i = bns_index

    # Load coordinates w/ errors
    RA = coord.Angle(raw[i][1], u.hourangle)
    errRA = coord.Angle(raw[i][2], u.arcsec)
    dec = coord.Angle(raw[i][3], u.deg)
    errdec = coord.Angle(raw[i][4], u.arcsec)
    dist = raw[i][9]
    errdist = raw[i][10]

    # Load proper motion w/ errors
    pm_RA = raw[i][5]
    errpm_RA = raw[i][6]
    pm_dec = raw[i][7]
    errpm_dec = raw[i][8]

    # Load merger time
    t_merger = raw[i][11]

    return t_merger, RA, errRA, dec, errdec, dist, errdist, pm_RA, errpm_RA, pm_dec, errpm_dec

def gen_rand_positions(RA, errRA, dec, errdec, dist, errdist, n):

    rand_RAs = np.random.normal(loc=RA.deg, scale=errRA.deg, size=n)
    rand_decs = np.random.normal(loc=dec.deg, scale=errdec.deg, size=n)
    rand_dist = trunc_normal(loc=dist, scale=errdist, size=n)
    
    return rand_RAs, rand_decs, rand_dist

def gen_rand_pm(pm_RA, errpm_RA, pm_dec, errpm_dec, n):

    rand_pmRA = np.random.normal(loc=pm_RA, scale=errpm_RA, size=n)
    rand_pmdec = np.random.normal(loc=pm_dec, scale=errpm_dec, size=n)

    return rand_pmRA, rand_pmdec
    
def gen_rand_vR(RA, dec, dist, pm_RA, pm_dec, LSRframe = False):
    
    # BNS coordinates
    # [here we compute the velocity in galactocentric coordinates; the routine subtracts the Sun's velocity]
    equatorials = coord.SkyCoord(RA*u.deg, dec*u.deg, dist*u.kpc, pm_ra_cosdec = pm_RA*u.mas/u.yr, pm_dec = pm_dec*u.mas/u.yr, radial_velocity = 0*u.km/u.s) 

    galactocentrics = equatorials.transform_to(coord.Galactocentric(representation_type = 'spherical'))

    galcen_x_bns = np.stack((galactocentrics.x.value, galactocentrics.y.value, galactocentrics.z.value), axis=-1)
    galcen_v_bns = np.stack((galactocentrics.v_x.value, galactocentrics.v_y.value, galactocentrics.v_z.value), axis=-1) #proper motion wrt. LSR + v_LSR

    # Sun coordinates and velocities
    x_sun = -8.122 # kpc
    z_sun = 20.8e-3 # kpc
    galcen_x_sun = np.tile(np.array([x_sun, 0, z_sun]), (len(galcen_x_bns),1)) # np.array([x_sun, 0, z_sun])
    galcen_v_sun = np.tile(np.array([12.9, 245.6, 7.78]), (len(galcen_x_bns),1)) # np.array([12.9, 245.6, 7.78]) # km/s

    
    # Line-of-sight vector
    galcen_x_los = galcen_x_bns - galcen_x_sun 
    galcen_x_los /= np.linalg.norm(galcen_x_los, axis=1, keepdims=1)

    # BNS transverse velocity
    galcen_vt_bns = galcen_v_bns - v_along_n(galcen_v_bns,galcen_x_los) 

    # BNS circular velocity
    R_bns = np.linalg.norm((galcen_x_bns[:,0],galcen_x_bns[:,1]), axis=0)
    v_circ = vcirc(McMillan17, R_bns*u.kpc)

    # BNS circular velocity vector
    i = np.c_[galcen_x_bns[:,:2], np.zeros(len(galcen_x_bns))]
    i /= np.linalg.norm(i, axis=1, keepdims=1)
    j = np.c_[np.zeros((len(galcen_x_bns),2)), np.ones(len(galcen_x_bns))]

    k = np.cross(i,j) # invertire l'ordine se la terna e' sinistrorsa

    galcen_v_lsr = k * v_circ[np.newaxis].T

    # LSR velocity
    galcen_vr_lsr = v_along_n(galcen_v_lsr,galcen_x_los)
    galcen_vt_lsr = galcen_v_lsr - galcen_vr_lsr
    
    # Correct for LSR motion (if wanted)
    if LSRframe == True:
        # Remove LSR motion from BNS motion
        galcen_vt_bns -= galcen_vt_lsr

    # BNS radial velocity
    cos_phi = uniform(-1, 1, len(galcen_vt_bns))
    phi = np.arccos(cos_phi)

    galcen_vr_bns = np.linalg.norm(galcen_vt_bns, axis=1) / np.tan(phi)

    if LSRframe == True:    
        galcen_vt_bns += galcen_vt_lsr
        galcen_vr_bns += np.linalg.norm(galcen_vr_lsr, axis=1)

    radial_velocity = galcen_vr_bns - np.linalg.norm(v_along_n(galcen_v_sun,galcen_x_los), axis=1)
    
    # BNS velocity wrt. LSR    
    vt_LSR, vr_lsr = np.linalg.norm(galcen_vt_bns-galcen_vt_lsr, axis=1), galcen_vr_bns-np.linalg.norm(galcen_vr_lsr, axis=1)
    
    return radial_velocity 
    
def gen_rand_vel(pm_RA, errpm_RA, pm_dec, errpm_dec, vR, errvR, n):

    rand_pmRA = np.random.normal(loc=pm_RA, scale=errpm_RA, size=n)
    rand_pmdec = np.random.normal(loc=pm_dec, scale=errpm_dec, size=n)
    rand_vR = np.random.normal(loc=vR, scale=errvR, size=n)

    return rand_pmRA, rand_pmdec, rand_vR
    
def get_offsets(LSRframe=False, n=10000, prnt=False):
    
    # Load BNS raw inputs
    
    raw = np.genfromtxt('inputs/galBNS_inputs', dtype=('U25', 'U25', float, 'U25', float, float, float, float, float, float, float, float))
    IDs = raw['f0']
    
    # Prepare offsets storage
    
    offsets_0 = np.zeros(3)
    offsets_1 = np.zeros(3)
    offsets_2 = np.zeros(3)
    offsets_3 = np.zeros(3)
    offsets_4 = np.zeros(3)
    offsets_5 = np.zeros(3)
    
    for ii in range(len(raw)):
        
        ID = IDs[ii]
        if prnt==True:
            print('>>> ', ID,' <<<\n')
        
        if ID == 'B2127+11C':
            
            # Get inputs  
            
            t_merger,RA,errRA,dec,errdec,dist,errdist,pm_RA,errpm_RA,pm_dec,errpm_dec,vR,errvR = [215,
                                                                                      coord.Angle('21:29:58.33', u.hourangle),
                                                                                      coord.Angle(0, u.arcsec),
                                                                                      coord.Angle('12:10:01.20', u.deg),
                                                                                      coord.Angle(0, u.arcsec),
                                                                                      10.22,0.13,
                                                                                      -0.63,0.01,
                                                                                      -3.80,0.01,
                                                                                      -106.76,0.25]

            # Generate n coordinates with Monte-Carlo
            
            rand_RAs, rand_decs, rand_dist = gen_rand_positions(RA, errRA, dec, errdec, dist, errdist, n)

            # Generate n proper motions & radial velocities with Monte-Carlo
            
            rand_pmRA, rand_pmdec, rand_vR = gen_rand_vel(pm_RA, errpm_RA, pm_dec, errpm_dec, vR, errvR, n)
            
        elif ID == 'J0514-4002A':
            
            # Get inputs            
            
            t_merger,RA,errRA,dec,errdec,dist,errdist,pm_RA,errpm_RA,pm_dec,errpm_dec,vR,errvR = [507733,
                                                                                      coord.Angle('05:14:06.76', u.hourangle),
                                                                                      coord.Angle(0, u.arcsec),
                                                                                      coord.Angle('-40:02:47.60', u.deg),
                                                                                      coord.Angle(0, u.arcsec),
                                                                                      11.33,0.19,
                                                                                      2.12,0.01,
                                                                                      -0.63,0.01,
                                                                                      320.30,0.25]

            # Generate n coordinates with Monte-Carlo
            
            rand_RAs, rand_decs, rand_dist = gen_rand_positions(RA, errRA, dec, errdec, dist, errdist, n)

            # Generate n proper motions & radial velocities with Monte-Carlo
            
            rand_pmRA, rand_pmdec, rand_vR = gen_rand_vel(pm_RA, errpm_RA, pm_dec, errpm_dec, vR, errvR, n)
        
        else:
            
            # Get inputs
            
            t_merger, RA, errRA, dec, errdec, dist, errdist, pm_RA, errpm_RA, pm_dec, errpm_dec = get_inputs(raw, ii)

            # Generate n coordinates with Monte-Carlo
            
            rand_RAs, rand_decs, rand_dist = gen_rand_positions(RA, errRA, dec, errdec, dist, errdist, n)

            if  ID == 'J0737-3039A':
                dist_x, dist_cdf = np.genfromtxt('inputs/J0737-3039A_cdf.txt').T
                interp_cdf = interp1d(dist_cdf, dist_x)
                uu = uniform(0,1,n)
                rand_dist = interp_cdf(uu)
            
            elif ID == 'J1756-2251' :
                dist_x, dist_cdf = np.genfromtxt('inputs/J1756-2251_cdf.txt').T
                interp_cdf = interp1d(dist_cdf, dist_x)
                uu = uniform(0,1,n)
                rand_dist = interp_cdf(uu)
            
            # Generate n proper motions with Monte-Carlo
            
            rand_pmRA, rand_pmdec = gen_rand_pm(pm_RA, errpm_RA, pm_dec, errpm_dec, n)

            # Generate radial velocities
            
            rand_vR = gen_rand_vR(rand_RAs, rand_decs, rand_dist, rand_pmRA, rand_pmdec, LSRframe)
            
        # Get positions and velocities in equatorial and galactocentric coordinates

        equatorials = coord.SkyCoord(rand_RAs*u.deg, 
                                     rand_decs*u.deg, 
                                     rand_dist*u.kpc, 
                                     pm_ra_cosdec = rand_pmRA*u.mas/u.yr,
                                     pm_dec = rand_pmdec*u.mas/u.yr,
                                     radial_velocity = rand_vR*u.km/u.s
                                     )

        galactocentrics = equatorials.transform_to(coord.Galactocentric(representation_type = 'spherical'))

        # Integrate the orbits

        if t_merger >= 14e3:
            t_merger = 2735
            
        foo = np.sort([85, 2735, 1457, 301, 215]) * u.Myr
        times = np.sort(np.append(np.linspace(0, 2740, 275),foo.value)) * u.Myr
        orb = Orbit(galactocentrics)
        orb.integrate(times, MWPotential2014, numcores=4)

        if prnt == True:
            # Plot offsets

            offsets = np.sqrt(orb.R(t_merger*u.Myr)**2 + orb.z(t_merger*u.Myr)**2)

            quartiles = np.round(np.percentile(offsets, [25,50,75]),2)
            errors = [[quartiles[1]-quartiles[0]], [quartiles[2]-quartiles[1]]]
            loc_quartiles = np.percentile(np.sqrt(galactocentrics.x**2 + galactocentrics.y**2 + galactocentrics.z**2), [25,50,75])/u.kpc

            plt.hist(offsets, bins=np.linspace(0,50,100))
            plt.plot([loc_quartiles[0],loc_quartiles[0]], [0.1,1e3], c='0', ls=':')
            plt.plot([loc_quartiles[1],loc_quartiles[1]], [0.1,1e3], c='0', label=('initial location'))
            plt.plot([loc_quartiles[2],loc_quartiles[2]], [0.1,1e3], c='0', ls=':')
            plt.plot([quartiles[0],quartiles[0]], [0.1,1e3], c='red', ls=':')
            plt.plot([quartiles[1],quartiles[1]], [0.1,1e3], c='red', label=('final location'))
            plt.plot([quartiles[2],quartiles[2]], [0.1,1e3], c='red', ls=':')
            plt.title('1st, 2nd, 3rd quartiles: '+str(quartiles[0])+', '+str(quartiles[1])+', '+str(quartiles[2])+' kpc')
            plt.xlabel('offset [kpc]')
            plt.ylabel('#')
            plt.ylim([1,1000])
            plt.xlim([0,50])
            plt.yscale('log')
            plt.legend()

            plt.show()
        
        # Save offsets
        
        offsets_0 = np.vstack((offsets_0, np.array([orb.R(times[0]), orb.z(times[0]), orb.phi(times[0])]).T))
        offsets_1 = np.vstack((offsets_1, np.array([orb.R(foo[0]), orb.z(foo[0]), orb.phi(foo[0])]).T))
        offsets_2 = np.vstack((offsets_2, np.array([orb.R(foo[1]), orb.z(foo[1]), orb.phi(foo[1])]).T))
        offsets_3 = np.vstack((offsets_3, np.array([orb.R(foo[2]), orb.z(foo[2]), orb.phi(foo[2])]).T))
        offsets_4 = np.vstack((offsets_4, np.array([orb.R(foo[3]), orb.z(foo[3]), orb.phi(foo[3])]).T))
        offsets_5 = np.vstack((offsets_5, np.array([orb.R(foo[4]), orb.z(foo[4]), orb.phi(foo[4])]).T))
        
    return offsets_0[1:], offsets_1[1:], offsets_2[1:], offsets_3[1:], offsets_4[1:], offsets_5[1:]
    
def save_offsets(wLSR=False, n=10000):
    
    offsets_0, offsets_1, offsets_2, offsets_3, offsets_4, offsets_5 = get_offsets(n=n)
    offsets = np.vstack((offsets_0, offsets_1, offsets_2, offsets_3, offsets_4, offsets_5))
    
    tm = np.array([0, 85, 215, 301, 1457, 2735])
    tm_tiled = np.repeat(tm, len(offsets_0)).reshape((len(offsets),1))
    
    ids = np.genfromtxt('inputs/galBNS_inputs', usecols=(0), dtype=('U25'))
    ids_tiled = np.tile(np.repeat(ids,len(offsets_0)/9),6).reshape((len(offsets),1))
    
    tobesaved = np.hstack((ids_tiled, tm_tiled, offsets))
    header = 'Pulsar --- time[Myr] --- R[kpc] --- z[kpc] --- phase[rad]'
    
    np.savetxt('offsets_noLSR.txt', tobesaved, header=header, fmt='%s')
    
    if wLSR==True:
        
        offsets_0, offsets_1, offsets_2, offsets_3, offsets_4, offsets_5 = get_offsets(LSRframe=True, n=n)
        offsets = np.vstack((offsets_0, offsets_1, offsets_2, offsets_3, offsets_4, offsets_5))
        
        tobesaved = np.hstack((ids_tiled, tm_tiled, offsets))
        np.savetxt('offsets_wLSR.txt', tobesaved, header=header, fmt='%s')

    return print('Done!')
    
if __name__ == '__main__':
    save_offsets(wLSR=True)