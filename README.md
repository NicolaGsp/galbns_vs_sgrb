Repository of codes and data used in: 
*"The Galactic neutron star population II - Systemic velocities and merger locations of binary neutron stars"*, [Gaspari et al., MNRAS 527, 1101-1113](https://ui.adsabs.harvard.edu/abs/2024MNRAS.527.1101G/abstract).

The repository do not contain the Milky Way 3D image (i.e. the content of /bulgebar and /disk) because of their size (~20GB). These files can be produced by running MW_image.py (beware that it will take ~hours), but we are glad to provide them upon request.