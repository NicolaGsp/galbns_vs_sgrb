import numpy as np
from numpy.random import uniform, choice, randint

from MW_photometry import interp_disk, interp_bbar, get_MW_image, get_Re, arcsec_to_kpc

def unison_shuffle(a, b):
    p = np.random.permutation(len(a))
    return a[p], b[p]

def xy_to_px(x, y, redshift, px_scale):
    
    grid_len = np.round(40/arcsec_to_kpc(redshift, px_scale),0)
    x = (20. +x)/arcsec_to_kpc(redshift, px_scale)
    y = (20. -y)/arcsec_to_kpc(redshift, px_scale)
    xy = np.array([x,y])
    #xy = (xy +20)/arcsec_to_kpc(redshift, px_scale)
    xy[xy<0] = 0
    xy[xy>grid_len] = grid_len
    
    return xy.astype(int)

def get_normoffs_flight(redshift=0.5, px_scale=0.05, psf_fwhm=0.1, N_inc=10, N_phi=20, wLSR=False, wBband=False):
    
    # set MW image

    MW_disk = interp_disk(B_band=wBband)
    MW_bulge = interp_bbar(B_band=wBband)

    # upload offsets
    
    if wLSR == False:
        raw_offcat = np.genfromtxt('outputs/offsets_noLSR.txt', usecols=(0,1), dtype=["U25",float])
        raw_offs = np.genfromtxt('outputs/offsets_noLSR.txt', usecols=(2,3,4))
    elif wLSR == True:
        raw_offcat = np.genfromtxt('outputs/offsets_wLSR.txt', usecols=(0,1), dtype=["U25",float])
        raw_offs = np.genfromtxt('outputs/offsets_wLSR.txt', usecols=(2,3,4))        
        
    # upload BNS IDs and merger times

    raw_bnscat = np.genfromtxt('inputs/galBNS_inputs', usecols=(0,11), dtype=['U25', float])
    t_merger = raw_bnscat['f1']

    # select only the offsets at the merger time of each BNS

    mergers_indx = np.zeros(len(raw_offcat), dtype=bool)
    for ii in range(len(raw_bnscat[t_merger < 14e3])):
        mergers_indx += (raw_offcat == raw_bnscat[t_merger < 14e3][ii])

    mergers = raw_offs[mergers_indx]
    IDs = raw_offcat['f0'][mergers_indx]
    N_bns = len(mergers)

    # shuffle and reshape the offset array to the optimal shape for the next cycle

    mergers, IDs = unison_shuffle(mergers, IDs)
    mergers = mergers.reshape((N_inc, N_phi, int(N_bns/(N_inc*N_phi)), 3))

    # set inclination and phase bins

    thetas = np.arccos(np.linspace(1,0,N_inc)).round(2) # actual bins
    phases = (np.linspace(0, 0.5*np.pi, N_phi) + np.deg2rad(-27)).round(2) # np.linspace(0, np.pi, N_phi).round(2)

    # compute normalized offsets

    normoff = np.empty(0)
    flight = np.empty(0)

    for ll in range(N_inc):
        i = thetas[ll]
        offs = mergers[ll]

        for mm in range(N_phi):
            phi = phases[mm]

            I_MW, mu_MW = get_MW_image(MW_disk, MW_bulge, i, phi, redshift, px_scale, psf_fwhm, prnt=False)
            R_e = get_Re(I_MW, i, phi, redshift, px_scale, toll=1.)

            R, z = mergers[ll,mm,:,:2].T
            iota = uniform(size=len(R)) *2*np.pi
            x, y = R*np.cos(iota), R*np.sin(iota)
            y_proj = y*np.cos(i) + z*np.sin(i)
            x_px, y_px = xy_to_px(x,y_proj,redshift,px_scale)

            R_proj = np.linalg.norm([x, y_proj], axis=0)
            R_norm = R_proj / R_e
            normoff = np.concatenate((normoff, R_norm))

            Maglim = 25
            I_galaxy = I_MW[mu_MW < Maglim]
            I_merger = I_MW[y_px,x_px]

            for foo in I_merger:
                flight = np.append(flight, np.sum(I_galaxy[I_galaxy<foo])/np.sum(I_galaxy))

    if wLSR == False:
        A = '_noLSR'
    elif wLSR == True:
        A = '_wLSR'
        
    if wBband == False:
        B = '_Iband'
    elif wBband == True:
        B = '_Bband'
        
    header = 'z:'+str(redshift)+' --- px scale:'+str(px_scale)+' --- psf fwhm:'+str(psf_fwhm)
    
    np.savetxt('normoffs'+A+B+'.txt', np.array([IDs, normoff]).T, fmt='%s', header=header)
    np.savetxt('flight'+A+B+'.txt', np.array([IDs, flight]).T, fmt='%s', header=header)

    return print('Done!')

if __name__ == '__main__':
    #get_normoffs_flight(wLSR=False, wBband=False)
    #get_normoffs_flight(wLSR=True, wBband=False)
    get_normoffs_flight(wLSR=False, wBband=True)
    get_normoffs_flight(wLSR=True, wBband=True)