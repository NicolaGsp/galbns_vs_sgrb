import numpy as np
import matplotlib.pyplot as plt
import glob

from scipy.interpolate import RegularGridInterpolator, interp1d
from scipy.ndimage import gaussian_filter

import astropy.units as u
from astropy.cosmology import LambdaCDM
from astropy.coordinates import Angle
from photutils.isophote import EllipseGeometry, Ellipse

from skimage.measure import block_reduce

from MW_image import model_arms

def interp_disk(B_band=False):
    
    if B_band == False:
        file = 'disk/disk_Iband_*'
    elif B_band == True:
        file = 'disk/disk_Bband_*'
        
    filenames = glob.glob(file)
    filenames.sort()
    
    inc = np.array([])
    light = []
    for name in filenames:
        inc = np.append(inc, float(name[16:-4]))
        light_temp = np.genfromtxt(name, usecols=(2))
        n_ltemp = np.sqrt(len(light_temp)).astype(int)
        light.append(light_temp.reshape((n_ltemp,n_ltemp)).T)
    light = np.array(light)
    
    x = y = np.linspace(-20,20,n_ltemp)
    
    return RegularGridInterpolator((inc,x,y), light)

def interp_bbar(B_band=False):
    
    if B_band == False:
        file = 'bulgebar/bbar_Iband_*'
    elif B_band == True:
        file = 'bulgebar/bbar_Bband_*'
    
    filenames = glob.glob(file)
    filenames.sort()
    
    inc = np.array([])
    phase = np.array([])
    light = []
    for name in filenames:
        inc_temp, phase_temp = [float(i) for i in name[20:-4].split('_')]
        
        if inc.size == 0:
            inc = np.append(inc, inc_temp)
            light_temp = []
        elif np.any(inc_temp==inc) == False:
            inc = np.append(inc, inc_temp)
            light.append(light_temp)
            light_temp = []
            
        if phase.size == 0 or np.any(phase_temp==phase) == False:
            phase = np.append(phase, phase_temp)
        
        foo = np.genfromtxt(name, usecols=(2))
        n_foo = np.sqrt(len(foo)).astype(int)
        light_temp.append(foo.reshape((n_foo,n_foo)).T)
    light.append(light_temp)
    light = np.array(light)
    
    x = y = np.linspace(-20,20,n_foo)
    
    return RegularGridInterpolator((inc,phase,x,y), light)

def MW_arms(i, phi, N_grid, grid_width=20, B_band=False):
    arms = model_arms(i, phi, grid_width, N_grid, B_band=B_band, prnt=False)
    return np.asarray(np.split(arms, N_grid))
    
def Lsun2mag(intensity, Magsun, redshift):
    
    D_L = LambdaCDM(H0=70, Om0=0.3, Ode0=0.7).luminosity_distance(redshift).value * 1e6 # pc    
    a = -2.5*np.log10(intensity)
    b = 5*np.log10(D_L/10)
    c = -2.5*np.log10(1+redshift)
    
    return a + b + c + Magsun

def arcsec_to_kpc(redshift, px_scale=1):
    
    scale = Angle(px_scale*u.arcsec).rad # pixel scale in rad px^-1
    D_A = LambdaCDM(H0=70, Om0=0.3, Ode0=0.7).angular_diameter_distance(redshift).value * 1e3 # kpc
    
    return scale * D_A
    
def get_MW_image(MW_disk, MW_bulge, i, phi, redshift, px_scale, psf_fwhm, prnt=False, Maglim=25, B_band=False):
       
    # set grid
    n_grid_min = 1e3 # minimum number of points on the interpolation grid (must be divisible by kpx_per_px)
    kpc_per_px = arcsec_to_kpc(redshift, px_scale)
    eta = np.ceil(n_grid_min*kpc_per_px/40).astype(int)
    x_grid = y_grid = np.arange(-20, 20, kpc_per_px/eta)
    xs, ys = np.meshgrid(x_grid, y_grid)

    # get resolved image
    I_MW_resolved = MW_disk((i,xs,ys)) + MW_arms(i,phi,len(x_grid),B_band=B_band) + MW_bulge((i,phi%np.pi,xs,ys))

    # convolve with gaussian PSF
    psf_sigma = psf_fwhm / np.sqrt(8*np.log(2))
    I_MW_wpsf = gaussian_filter(I_MW_resolved, (psf_sigma/px_scale)*eta)

    ## bin by pixel scale
    I_MW = block_reduce(I_MW_wpsf, block_size=(eta,eta), func=np.mean) # MW image in Lsun pc^-2
    pc2_per_arcsec2 = (arcsec_to_kpc(redshift)*1e3)**2
    
    mu_MW = Lsun2mag(I_MW*pc2_per_arcsec2, 4.51, redshift) # MW image in mag
    
    if prnt==True:
        side = 20 / arcsec_to_kpc(redshift)
        plt.figure(figsize=(12,10))
        plt.imshow(mu_MW, cmap='gray_r', vmax=Maglim, extent=(-side,side,-side,side))
        plt.xlabel('[arcsec]')
        plt.ylabel('[arcsec]')
        plt.title('z = '+str(redshift)+' ::: i = '+str(np.rad2deg(i).round(2))+' [deg]'+' ::: Px scale = '+str(px_scale)+' ["/px] ::: PSF FWHM = '+str(psf_fwhm)+' ["]')
        cbar = plt.colorbar()
        cbar.ax.set_ylabel(r'mag arcsec$^{-2}$')
        plt.show()
        
    #I_MW[mu_MW>Maglim] = 0
    
    return I_MW, mu_MW

def get_Re(I_MW, i, phi, redshift, px_scale, toll=1.):
    
    bbar_phase = np.deg2rad(-27)
    psi = bbar_phase - phi
    
    if np.deg2rad(88) < abs(psi) <= 0.5*np.pi:
        psi = np.sign(psi) * np.deg2rad(88)
    elif 0.5*np.pi < abs(psi) < np.rad2deg(92):
        psi = np.sign(psi) * np.deg2rad(92)
    
    a = 2.2/arcsec_to_kpc(redshift, px_scale) # a = 3.0 kpc
    c = 1.1/arcsec_to_kpc(redshift, px_scale) # c = 1.5 kpc
    b = 0.7/arcsec_to_kpc(redshift, px_scale) # b = 1.0 kpc
    a_proj = a * np.sqrt(np.cos(psi)**2 + np.sin(psi)**2*np.cos(i)**2)
    if a_proj < c:
        a_proj, c = c, 0.8*a_proj
        if c < b:
            c = b        
    eps = np.sqrt(1-(c**2/a_proj**2))

    pa = np.arctan(np.tan(psi)*np.cos(i)**6) # NGsp: cos is squared to enhance the PA flattening at high inc.
    if abs(i) > 1.1:
        pa = 0
    
    geometry_guess = EllipseGeometry(x0=len(I_MW)/2-0.5, y0=len(I_MW)/2-0.5, sma=a_proj, eps=eps, pa=pa)
    ellipse_guess = Ellipse(I_MW, geometry_guess)
    iso_guess = ellipse_guess.fit_isophote(a_proj, sclip=3, nclip=3)
    
    geometry = EllipseGeometry(iso_guess.x0, iso_guess.y0, iso_guess.sma, iso_guess.eps, iso_guess.pa)
    ellipse = Ellipse(I_MW, geometry)
    isolist = ellipse.fit_image(sclip=3, nclip=3)
       
    flux_tot = isolist.tflux_e[1:]
    flux_frac = flux_tot/flux_tot[-1]
    sma = isolist.sma[1:]
    intens = isolist.intens[1:]
    pc2_per_arcsec2 = (arcsec_to_kpc(redshift)*1e3)**2
    mu = Lsun2mag(intens*pc2_per_arcsec2, 4.51, redshift)

    fit_error = (abs(np.sum(I_MW)-flux_tot[-1])*100/np.sum(I_MW)).round(1)
    if fit_error >= toll:
        print("!!!!! Fit lost "+str(fit_error)+"% of light on [i,phi]="+str(np.round((i,phi),2)))

    mu_to_flux = interp1d(mu, flux_frac)
    Re = interp1d(flux_frac, sma*arcsec_to_kpc(redshift, px_scale))
    
    return Re(mu_to_flux(25)/2).item()

if __name__ == '__main__':
    print('This script contains functions, it does nothing')