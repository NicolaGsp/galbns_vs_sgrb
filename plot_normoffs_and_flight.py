import numpy as np
from numpy.random import choice, randint, normal

import matplotlib.pyplot as plt

from scipy.stats import kstest

def plot_normoffs(filename):
    
    IDs = np.genfromtxt(filename, usecols=(0), dtype='U25')
    normoff = np.genfromtxt(filename, usecols=(1))
    raw_bnscat = np.genfromtxt('inputs/galBNS_inputs', usecols=(0,11), dtype=['U25', float])
    
    # load Fong+22 offsets

    e = np.array([2.59, 0.67, 9.34, 13.98, 0.78, 0.64])
    e_err = np.array([0.58, 0.02, 0.75, 0.03, 0.008, 0.03])
    l = np.array([2, 5.65, 0.89, 0.86, 0.37, 1.17, 5.17, 1.49, 3.08, 4.61, 0.29, 1.66, 0.61, 0.71, 3.42, 2.37, 4.24, 0.24, 0.49])
    l_err = np.array([0.03, 3.17, 0.083, 0.97, 0.077, 2.62, 0.37, 0.048, 0.055, 0.044, 0.14, 0.46, 0.14, 0.027, 0.2, 1.54, 0.008, 0.048, 0.18])
    u = np.array([0.18, 1.39, 1.11, 3.46, 1.19, 1.93, 1.41, 0.25, 1.50, 0.49])
    u_err = np.array([0.069, 0.3, 0.083, 0.12, 0.083, 0.19, 0.38, 0.068, 0.14, 0.18])
    w = np.concatenate((e,l,u))
    w_err = np.concatenate((e_err,l_err,u_err))

    # get random CF from Fong+22 offsets

    NN = 3000

    e_rand = np.array([]).reshape(0,len(e))
    l_rand = np.array([]).reshape(0,len(l))
    w_rand = np.array([]).reshape(0,len(w))

    for N in range(NN):
        e_rand = np.vstack((e_rand,normal(e, e_err)))
        l_rand = np.vstack((l_rand,normal(l, l_err)))
        w_rand = np.vstack((w_rand,normal(w, w_err)))

    e_rand[e_rand<0] = 2e-2
    l_rand[l_rand<0] = 2e-2
    w_rand[w_rand<0] = 2e-2

    # get random CF from our normalized offsets

    offs = np.array([]).reshape(0,5)

    for N in range(NN):
        offs_temp = np.empty(0)
        for ID in raw_bnscat['f0']:
            indx = (IDs==ID)
            if np.any(indx) == True:
                offs_temp = np.append(offs_temp, choice(normoff[indx]))
        offs = np.vstack((offs, offs_temp))

    offs[offs<1e-2]=1e-2
    offs[offs>1e2]=1e2
    
    # compute p-values
    
    pvalue = [[],[],[]]

    for ii in range(10000):
        foo = offs[randint(0,NN)]
        indx = randint(0,NN)
        pvalue[0].append(kstest(foo,l_rand[indx])[1])
        pvalue[1].append(kstest(foo,e_rand[indx])[1])
        pvalue[2].append(kstest(foo,w_rand[indx])[1])

    pvalue = np.array(pvalue)

    print('\np<0.05 for late-types: ',np.where(pvalue[0]>0.05)[0].size * 100/10000,'%')
    print('p<0.05 for early-types: ',np.where(pvalue[1]>0.05)[0].size * 100/10000,'%')
    print('p<0.05 for whole sample: ',np.where(pvalue[2]>0.05)[0].size * 100/10000,'%')

    plt.figure(figsize=(5,5))
    plt.hist(pvalue[0], bins=np.linspace(0,1,100), cumulative=True, density=True, ec='navy', histtype='step', label="Late-type")
    plt.hist(pvalue[1], bins=np.linspace(0,1,100), cumulative=True, density=True, ec='red', histtype='step', label='Early-type')
    plt.hist(pvalue[2], bins=np.linspace(0,1,100), cumulative=True, density=True, ec='black', histtype='step', label='Whole sample')
    plt.plot([0.05,0.05],[0,1],c='0',ls=':')
    plt.xlim([0,1])
    plt.ylim([0,1])
    plt.xlabel('p-value')
    plt.ylabel('Cumulative fraction')
    plt.grid()
    plt.legend(loc=4)
    plt.show(block=False)
    
    bins = np.logspace(-2,2,3000)

    plt.figure(figsize=(5,5))
    for N in range(NN):
        plt.hist(offs[N], bins=bins, cumulative=True, density=True, histtype='step', color='#09a307', alpha=0.02, lw=1, zorder=0)
        plt.hist(l_rand[N], bins=bins, cumulative=True, density=True, histtype='step', color='blue', alpha=0.02, lw=1, zorder=1)
        plt.hist(e_rand[N], bins=bins, cumulative=True, density=True, histtype='step', color='#ff3419', alpha=0.02, lw=1, zorder=2)
    plt.hist(np.median(np.sort(offs, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', ls="--", color='#1b6909', lw=1.5, zorder=3, label='Milky Way')
    plt.hist(np.median(np.sort(l_rand, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', color='navy', ls="-", lw=1.5, zorder=3, label='Late-type')
    plt.hist(np.median(np.sort(e_rand, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', color='#db0202', ls="-", lw=1.5, zorder=3, label='Early-type')
    plt.xlim((1e-1,50))
    plt.xscale('log')
    plt.ylim((1e-2,1))
    plt.ylabel('Cumulative fraction')
    plt.xlabel('Normalized offset')
    plt.grid()
    plt.legend(loc=4)

    plt.show(block=False)
    
    plt.figure(figsize=(5,5))
    for N in range(NN):
        plt.hist(offs[N], bins=bins, cumulative=True, density=True, histtype='step', color='#09a307', alpha=0.02, lw=1, zorder=0)
        plt.hist(w_rand[N], bins=bins, cumulative=True, density=True, histtype='step', color='black', alpha=0.02, lw=1, zorder=1)
    plt.hist(np.median(np.sort(offs, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', ls="--", color='#1b6909', lw=1.5, zorder=3, label=r'Milky Way')
    plt.hist(np.median(np.sort(w_rand, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', color='black', ls="-", lw=1.5, zorder=3, label=r'Whole sample')
    plt.xlim((1e-1,50))
    plt.xscale('log')
    plt.ylim((1e-2,1))
    plt.ylabel('Cumulative fraction')
    plt.xlabel('Normalized offset')
    plt.grid()
    plt.legend(loc=4)
    
    plt.show(block=False)
    
    return print('\nPlotted '+filename+'\n)
                 
def plot_flight(filename):
    
    IDs = np.genfromtxt(filename, usecols=(0), dtype='U25')
    flights = np.genfromtxt(filename, usecols=(1))
    raw_bnscat = np.genfromtxt('inputs/galBNS_inputs', usecols=(0,11), dtype=['U25', float])
    
    # load Fong+22 offsets

    e = np.array([0.33,0.,0.21,0.54])
    l = np.array([0.09,0.65,0.63,0.00,0.23,0.00,0.00,0.82,0.00,0.35,0.95])
    u = np.array([0.41,0.00,0.00,0.30])
    w = np.concatenate((e,l,u))

    flight = np.array([]).reshape(0,5)

    # get random CF from our normalized offsets

    NN = 3000
    
    for N in range(NN):
        flight_temp = np.empty(0)
        for ID in raw_bnscat['f0']:
            indx = (IDs==ID)
            if np.any(indx) == True:
                flight_temp = np.append(flight_temp, choice(flights[indx]))
        flight = np.vstack((flight, flight_temp))
    
    # compute p-values
    
    pvalue = [[],[],[]]

    for ii in range(NN):
        foo = flight[ii]
        pvalue[0].append(kstest(foo,l)[1])
        pvalue[1].append(kstest(foo,e)[1])
        pvalue[2].append(kstest(foo,w)[1])

    pvalue = np.array(pvalue)

    print('\np>0.05 for late-types: ',np.where(pvalue[0]>0.05)[0].size * 100/NN,'%')
    print('p>0.05 for early-types: ',np.where(pvalue[1]>0.05)[0].size * 100/NN,'%')
    print('p>0.05 for whole sample: ',np.where(pvalue[2]>0.05)[0].size * 100/NN,'%')

    plt.figure(figsize=(5,5))
    plt.hist(pvalue[0], bins=np.linspace(0,1,100), cumulative=True, density=True, ec='navy', histtype='step', label="Late-type")
    plt.hist(pvalue[1], bins=np.linspace(0,1,100), cumulative=True, density=True, ec='red', histtype='step', label='Early-type')
    plt.hist(pvalue[2], bins=np.linspace(0,1,100), cumulative=True, density=True, ec='black', histtype='step', label='Whole sample')
    plt.plot([0.05,0.05],[0,1],c='0',ls=':')
    plt.xlim([0,1])
    plt.ylim([0,1])
    plt.xlabel('p-value')
    plt.ylabel('Cumulative fraction')
    plt.grid()
    plt.legend(loc=4)
    plt.show(block=False)
    
    bins = np.logspace(-2,2,3000)

    plt.figure(figsize=(5,5))
    for N in range(NN):
        plt.hist(flight[N], bins=bins, cumulative=True, density=True, histtype='step', color='#09a307', alpha=0.02, lw=1)
    plt.hist(np.median(np.sort(flight, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', color='#1b6909', ls="--", lw=1.5, zorder=3, ec='#1b6909', label=r'Milky Way')
    plt.hist(l, histtype='step', cumulative=True, density=True, bins=bins, ec='navy', lw=1.5, label=r'Late-type')
    plt.hist(e, histtype='step', cumulative=True, density=True, bins=bins, ec='#db0202', lw=1.5, label=r'Early-type')
    plt.xlim((0,1))
    plt.ylim((1e-6,1))
    plt.ylabel('Cumulative fraction')
    plt.xlabel(r'$f_\textrm{light}$')
    plt.legend(loc=4)

    plt.show(block=False)
    
    plt.figure(figsize=(5,5))
    for N in range(NN):
        plt.hist(flight[N], bins=bins, cumulative=True, density=True, histtype='step', color='#09a307', alpha=0.02, lw=1)
    plt.hist(np.median(np.sort(flight, axis=1), axis=0), bins=bins, cumulative=True, density=True, histtype='step', color='#1b6909', ls="--", lw=1.5, zorder=3, ec='#1b6909', label=r'Milky Way')
    plt.hist(w, histtype='step', cumulative=True, density=True, bins=bins, ec='black', lw=1.5, label=r'Whole sample')
    plt.xlim((0,1))
    plt.ylim((1e-6,1))
    plt.ylabel('Cumulative fraction')
    plt.xlabel(r'$f_\textrm{light}$')    
    
    plt.show(block=False)
    
    return print('\nPlotted '+filename+'\n)

if __name__ == '__main__':
    plot_normoffs('normoffs_noLSR_Iband.txt')
    plot_flight('flight_noLSR_Iband.txt')