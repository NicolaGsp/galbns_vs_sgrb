import numpy as np
from scipy.spatial.transform import Rotation
from scipy.optimize import root_scalar

def get_xygrid(edge=20, N=1001, scale=-999):
    # Setting the non-rotated grid
    if scale < 0:
        x = y = np.linspace(-edge, edge, N) # kpc, grid points
    else:
        x = y = np.arange(-edge, edge, scale)
    xx, yy = np.meshgrid(x,y) # get grid
    grid = np.vstack((xx.flatten(), yy.flatten())).T # stacked coordinates    
    return grid

def rho_disk(x,y,z,re,ze):
    r = np.sqrt(x**2 + y**2)
    exp_r = np.exp(-r/re)
    exp_z = np.exp(-np.abs(z)/ze)
    return exp_r * exp_z

def rho_bar(x,y,z,x0,y0,z0,pa=27):
    theta = np.radians(pa)
    xr, yr = np.cos(theta)*x - np.sin(theta)*y, np.sin(theta)*x + np.cos(theta)*y
    xy = (xr/x0)**2 + (yr/y0)**2
    m = (xy**2 + (z/z0)**4)**(1/4)
    return np.exp(-0.5*m**2)

def Sigma_sersic(r,n,re):
    b = 2*n - (1/3) + (0.009876/n) # Prugniel&Simien97
    return np.exp(-b*(r/re)**(1/n))

def f(scale, I_bbar, dxdy, Lbbar):
    A = np.sum(np.asarray(I_bbar)*dxdy)
    return A*scale-Lbbar, A

def model_disk(i, phi=np.pi/2, grid_width=20, N_grid=1001, B_band=False):
    
    # Setting frame rotation
    
    R = Rotation.from_euler('ZX', [phi, i])
    R_inv = R.inv()
    
    # Setting (x,y,z) grid

    xys = get_xygrid(grid_width, N_grid) # (x,y) points
    dxdy = 2*grid_width * 2*grid_width / len(xys) # area step
    zs = np.linspace(-grid_width, grid_width, N_grid) # z points
    dz = 2*grid_width / N_grid # z step
    
    ############### Modelling the disk ###############

    re = 2.6 # kpc, scale radius
    ze = 0.3 # kpc, scale height

    arm_strenght = 0.15
    L_diskarms = 3e10 # Lsun, total luminosity of disk plus spiral arms
    if B_band==True:
        arm_strenght = 0.20
        L_diskarms = 0.67e10 # Lsun, total luminosity of disk plus spiral arms
    Ldisk = L_diskarms * (1-arm_strenght)

    I_disk = [] # intensity array
    for xy in xys: # looping through grid points in (x,y) 
        xyz = np.concatenate((np.tile(xy,(len(zs),1)), np.expand_dims(zs, axis=1)), axis=1) # add z coord
        xyz_rot = R.apply(xyz) # rotate grid
        I_xy = np.sum(rho_disk(xyz_rot[:,0],xyz_rot[:,1],xyz_rot[:,2],re,ze) *dz) # Lsun kpc^-2, intensity map evaluated at (x,y) (integrated along z i.e. mean value times base)
        I_disk.append(I_xy) # stack intensity at point

    # Next line gives the disk luminosity per grid's units area i.e. L_disk /dxdy
    I_disk = np.array(I_disk) * Ldisk/(4*np.pi*ze*re**2) # normalize with I_0
    print('Ltot_disk = ', np.sum(I_disk*dxdy*1e-10).round(2), 'x1e10 Lsun') # total intensity i.e. mean value times area
    
    I_disk *= 1e-6 # Convert from Lsun kpc^-2 to Lsun pc^-2
    
    return I_disk
    
def model_arms(i, phi=np.pi/2, grid_width=20, N_grid=1001, scale=-999, B_band=False, prnt=True):
    
    # Setting frame rotation
    
    R = Rotation.from_euler('ZX', [phi, i])
    R_inv = R.inv()
    
    # Setting (x,y,z) grid

    if scale < 0:
        xys = get_xygrid(grid_width, N_grid) # (x,y) points
        zs = np.linspace(-grid_width, grid_width, N_grid) # z points    
        dxdy = 2*grid_width * 2*grid_width / len(xys) # area step
        dz = 2*grid_width / len(zs) # z step
    else:
        xys = get_xygrid(grid_width, scale=scale) # (x,y) points
        zs = np.arange(-grid_width, grid_width, scale) # z points    
        dxdy = 2*grid_width * 2*grid_width / len(xys) # area step
        dz = 2*grid_width / len(zs) # z step
    
    ############### Modelling the arms ###############

    # uploading Ashely's files
    yarm_raw = -(np.genfromtxt('arms/reid_and_sprinkles_x.txt'))
    xarm_raw = -(np.genfromtxt('arms/reid_and_sprinkles_y.txt') -8.3)
    zarm_raw = np.genfromtxt('arms/reid_and_sprinkles_h.txt')
    Larm_raw = np.genfromtxt('arms/reid_and_sprinkles_bol.txt')

    # cutting out half galaxy, reflecting it, and rescaling the luminosity by Ltot and arm_strenght
    upper_half = np.where(yarm_raw<0)

    x_arm = np.hstack((xarm_raw[upper_half], -xarm_raw[upper_half]))
    y_arm = np.hstack((yarm_raw[upper_half], -yarm_raw[upper_half]))
    z_arm = np.hstack((zarm_raw[upper_half], -zarm_raw[upper_half]))
    L_arm = np.hstack((Larm_raw[upper_half], Larm_raw[upper_half]))
    
    arm_strenght = 0.15
    L_diskarms = 3e10 # Lsun, total luminosity of disk plus spiral arms
    if B_band==True:
        arm_strenght = 0.20
        L_diskarms = 0.67e10 # Lsun, total luminosity of disk plus spiral arms
    L_arm /= np.sum(L_arm) # normalizing arms luminosity
    L_arm *= L_diskarms * arm_strenght / dxdy # rescaling arms luminosity to L_diskarms * arm_strenght for grid's unit area

    # binning positions into xx/yy grid
    xyz_arm_rot = R_inv.apply(np.stack((x_arm, y_arm, z_arm), axis=-1))
    I_arms = np.zeros(int(N_grid**2))

    for ii in range(len(xyz_arm_rot)):
        indx_y = np.argmin(np.abs(xyz_arm_rot[ii,1]-zs))*N_grid
        slab_x = xys[indx_y:indx_y+N_grid,0]
        indx_x = np.argmin(np.abs(xyz_arm_rot[ii,0]-slab_x))
        indx_grid = indx_y + indx_x  
        I_arms[indx_grid] += L_arm[ii]
    if prnt==True:
        print('Ltot_arms = ', np.sum(I_arms*dxdy*1e-10).round(2), 'x1e10 Lsun') # total intensity i.e. mean value times area
    
    I_arms *= 1e-6 # Convert from Lsun kpc^-2 to Lsun pc^-2
    
    return I_arms
    
def model_bbar(i, phi, grid_width=20, N_grid=1001, B_band=False):
    
    # Setting frame rotation
    
    R = Rotation.from_euler('ZX', [phi, i])
    R_inv = R.inv()

    # Setting (x,y,z) grid

    xys = get_xygrid(grid_width, N_grid) # (x,y) points
    dxdy = 2*grid_width * 2*grid_width / len(xys) # area step
    zs = np.linspace(-grid_width, grid_width, N_grid) # z points
    dz = 2*grid_width / N_grid # z step
    
    ############### Modelling the bulge/bar ###############

    # Bar pars
    x0, y0, z0 = 2.05, 0.95, 0.73

    # Bulge pars
    n_b, re_b = 1.32, 0.64

    I_bbar = [] # intensity array
    for xy in xys: # looping through grid points in (x,y) 
        xyz = np.concatenate((np.tile(xy,(len(zs),1)), np.expand_dims(zs, axis=1)), axis=1) # add z coord
        xyz_rot = R.apply(xyz) # rotate grid
        #I_xy = np.sum(rho_bar(xyz_rot[:,0],xyz_rot[:,1],xyz_rot[:,2],x0,y0,z0)) *Sigma_sersic(np.linalg.norm(xy),n_b,re_b) *dz # Lsun kpc^-2, intensity map evaluated at (x,y) (integrated along z i.e. mean value times base)
        I_xy = np.sum(rho_bar(xyz_rot[:,0],xyz_rot[:,1],xyz_rot[:,2],x0,y0,z0)) *dz # Lsun kpc^-2, intensity map evaluated at (x,y) (integrated along z i.e. mean value times base)
        I_bbar.append(I_xy) # stack intensity at point

    # Find scale factor to get Ltot
    Lbbar = 1e10 # Lsun, total luminosity of the bar
    if B_band == True:
        Lbbar = 0.11e10 # Lsun, total luminosity of the bar
    scale = root_scalar(f, fprime=True, x0=10, args=(I_bbar, dxdy, Lbbar)).root

    I_bbar = np.array(I_bbar) *scale # Lsun kpc^-2, intensity map (integrated along z i.e. mean value times base)
    print('Ltot_bbar = ', np.sum(I_bbar*dxdy*1e-10).round(2), 'x1e10 Lsun') # total intensity i.e. mean value times area
    
    I_bbar *= 1e-6 # Convert from Lsun kpc^-2 to Lsun pc^-2
    
    return I_bbar    
    
def save_disk(N_inc=7, wB=False):
    
    print('\n############### Modelling the disk ###############')
    
    js = np.arccos(np.linspace(0,1,N_inc))
    
    for j in js:
        
        grid = get_xygrid()
        
        print('\nInclination: '+str(np.rad2deg(j).round(2))+' [deg]')        
        
        print('> I-band')         
        disk = model_disk(j)
        np.savetxt(
            'disk_Iband_'+str(j.round(2))+'.txt',
            np.concatenate(
                (grid, disk.reshape((len(grid),1))),
                axis=1),
            header='Inclination: '+str(j)+' [rad]')
            
        if wB == True:
            print('> B-band')        
            disk = model_disk(j, B_band=True)
            np.savetxt(
                'disk_Bband_'+str(j.round(2))+'.txt',
                np.concatenate(
                    (grid, disk.reshape((len(grid),1))),
                    axis=1),
                header='Inclination: '+str(j)+' [rad]')

    return print('\n############### Disk done! ###############\n')    
    
def save_bulgebar(N_inc=7, N_phi=13, wB=False):
    
    print('\n############### Modelling the bulge/bar ###############')
    
    js = np.arccos(np.linspace(0,1,N_inc))
    phis = np.arccos(np.linspace(-1,1,N_phi))

    for j in js:
    
        print('\nInclination: '+str(np.rad2deg(j).round(2))+' [deg]')
    
        for phi in phis:
        
            grid = get_xygrid()
                    
            print('\nPhase: '+str(np.rad2deg(phi).round(2))+' [deg]')

            print('> I-band')
            bbar = model_bbar(j, phi)
            np.savetxt(
                'bbar_Iband_'+str(j.round(2))+'_'+str(phi.round(2))+'.txt',
                np.concatenate(
                    (grid, bbar.reshape((len(grid),1))),
                    axis=1),
                header='Inclination: '+str(j)+' [rad] - Phase: '+str(phi)+' [rad]')

            if wB == True:
                print('> B-band')
                bbar = model_bbar(j, phi, B_band=True)
                np.savetxt(
                    'bbar_Bband_'+str(j.round(2))+'_'+str(phi.round(2))+'.txt',
                    np.concatenate(
                        (grid, bbar.reshape((len(grid),1))),
                        axis=1),
                    header='Inclination: '+str(j)+' [rad] - Phase: '+str(phi)+' [rad]')

    return print('\n############### Bulge/bar done! ###############\n')    
    
if __name__ == '__main__':
    save_disk(wB=True)
    #save_bulgebar(wB=True)    